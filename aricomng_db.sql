-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: aricomng_db
-- ------------------------------------------------------
-- Server version	5.7.26-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` (`id`, `title`, `description`) VALUES (1,'Free Medical Outreach for Orphans','ARI team members volunteered at Orphans Corpâ€™s medical outreach for 200 orphans in December 2016 in Kano State. The target was to reach 200 orphans between the ages of 2 and 18 but over 200 orphans and children were given medication. The outreach was well publicized through radio jingles, interviews and social media. The kids were tested for malaria, typhoid, malnutrition etc. Antimalaria drugs, Antibiotics and mosquito nets were shared among patients and participants.'),(2,'Rigasa Medical Outreach','Adversity Relief Initiative (ARI) organized a medical outreach in partnership with the Society for Family Health (SFH) on the 18th February 2017. The medical outreach, which was carried out in Rigasa Community in Igabi LGA, Kaduna State which is adjudged to be one of the largest community with a population of 3 million people. The medical outreach includes free insecticide nets to repel mosquitoes, drugs for common illnesses, free HIV test, blood sugar level checks, malaria and hepatitis tests, deworming as well as counselling. With the help of SFH the foundation was able to carry out Cervical Cancer screening for 600 women. It has total number of 815 people in attendance whom came out to be checked. The outreach targeted mainly women and children turned out to be for all the members of the community including men because it was later found out the men too need the free medical treatment. Medical Lab Test Kits were used to test for Cervical Cancer, HIV/AIDS, Malaria etc. 3 cervical cancer cases were found while 4 Females were found to be HIV positive. A further test showed that the virus was transferred to one of the Female Patients by her ignorant husband. All 7 patients were referred to Barau Dikko Specialist Hospital for further medical assistance and examination. At the end, each woman went home with a free mosquito net as this will help in preventing her and her family from mosquito bites which will result in them having malaria.'),(3,'VVF CENTRE VISITATION/OUTREACH','On 23rd May 2017 Adversity Relief Initiative (ARI) paid a visit to the VVF Centre at Murtala Muhammad Specialist Hospital which is the largest of its kind in West Africa to mark the International Day to End Fistula (IDEOF) world day which holds on the 23rd May of every year. Members contributed money which was used to buy consumables such as Gloves, Gauze, Antiseptics, Detergents and IV Fluids.'),(4,'SWEATER DISTRIBUTION IN KANO STATE','This outreach took place on the 1st of July 2017. The idea for this outreach which has since become an annual event on the calendar of ARI came up as a result of subsequent meetings and discussions by members considering how cold it gets in December and the number of less privileged and homeless kids roaming around the streets equally half naked. Members made contributions in other to see that this outreach is carried out successfully, which will help in getting cardigans, head warms, socks, shirts, trousers etc. This outreached started with the\r\nIDPs which are located at Hotoro behind ARTV in Kano State, the team proceeded to the handicapped home at Tudun\r\nMaliki in Kano State. Over 300 people benefitted from the sweater distribution.'),(5,'KADUNA STATE CLOTH DRIVE','On the 19th of November 2017 ARI team carried out a clothing distribution at Kwata Makera Community in Kaduna State, where over 200 residents benefitted from this outreach.'),(6,'RIMIN GADO MEDICAL OUTREACH','ARI in partnership with Societal Healthcare Organization (SHO) organized a medical outreach in July. 8 Medical Doctors, 5 Pharmacists and several volunteers participated in the outreach which took place in front of the District Heads House in Rimin Gado Local Government. A huge number of Elderly persons where attended to with ailments ranging from high blood pressure, diabetes and malaria. Eye problems such as cataract and macular degeneration were also detected in a few patients. A huge number of patients were referred to Murtala Muhammad Specialist Hospital for further investigation.');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`id`, `name`, `username`, `password`) VALUES (1,'Umar Sunusi Maitalata','maitalata','1db089a9f84e14e95f11dcb5c57fa10d60aa84bb'),(2,'Munzali Garba','munzali','221c6265161c9ff0704a27d48e26aa81e14acc05');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `position` varchar(100) NOT NULL,
  `bio` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` (`id`, `name`, `position`, `bio`) VALUES (1,'Abdullahi Umar','Event Planner','Abdullahi Umar is an active citizen, a graduate of MSc Human Resources Management, U.S.A Delaware &amp; BSc Mass Communication BUK. He has a clear, logical mind with a practical approach to problem solving and a drive to see things through to completion. He has more than four yearsâ€™ experience in managing and leading teams across multiple sectors. He enjoys overcoming challenges and has a genuine interest in community development services and achieving sustainable impact.'),(2,'Fatima Mahdi Shehu','Logistics','Fatima is a graduate of Computer Science; she is currently working with a private pharmaceutical company. She has worked with various Nongovernmental organizations, private organizations and a few entrepreneurs. She has held leadership position in different sectors and has been working in the health sector for years, as an Auditor, Network Administrator, Software team and lots more. Fatima is a philanthropist and is willing to reach out to less privileged ones'),(3,'Zulfaat Abubakar Muhammad','PRO','Zulfaat is a civil servant, A graduate of B. Ed English and PGD Mass Com from Bayero University Kano, she is currently a journalist with Kano State Television as a reporter and a caster. Self-Motivated a good communication skill, also has an extensive experience working on television and radio, fluent in both Hausa and English.'),(4,'Munzali Garba','IT Officer II','Munzali is an IT consultant/entrepreneur; hegraduated from Ahmadu Bello University Zaria and has a passion for societal development, child education and women empowerment.'),(5,'Najib Adamu Haruna Esq. (LL. B, BL)','Legal Adviser','Najib is a lawyer and human rights activist who provide legal representation services in dispute resolution and litigation, real estate, corporate &amp; investment law.'),(6,'Sadiq Tajudeen','Chairperson','Sadiq is the president of Adversity Relief Initiative, a not for profit organization that focuses on providing aids and support to less privilege, child welfare, social inclusion and youth advocacy. Having lost his father at a very young age and singularly raised by his struggling mother, life wasnâ€™t always rosy, which inspired him to devote his time to causes that affect children and youth in general, on the strength of empathy, compassion and social inclusion. Sadiq has participated in several youth advocacy programs, such as the 20th session of the Youth Assembly, at the United Nations Head Quarters, New York, USA, which presented him with an enormous opportunity to contribute to the conversation on youth engagement and participation in achieving an inclusive and sustainable future where no child is left behind Sadiq is a successful entrepreneur who has founded several successful businesses. He is the Chairman of STAJ Integrated Investment and the CEO of STAJ Collections to name a few. He is a graduate of Bayero University where he obtained a degree in Economics.'),(7,'Sajida Muhammad Abdulrahman','Outreach Co-ordinator + Administrative and Strategic Officer','Sajida is a young, vibrant and passionate Medical Doctor who is a graduate of the prestigious Khartoum College of Medical Sciences (IBN SINA UNIVERSITY), currently serving at Dutse General Hospital, Jigawa. Sheâ€™s the chairperson HOSPITAL FRIENDS Muhammad Abdullahi Wase Specialist Hospital Kano. Dr Sajida is also a Member of local and multinational organizations whose objectives are to provide, promote and improve health care delivery within the country'),(8,'Anas Yushaâ€™u Ango','Secretary + Research and Continuity Officer','Anas is a lecturer at Bayero University Kano; he holds a degree and an MSc in Accounting and Finance from University of Essex, UK. He is dependable with very good judgement and his mature outlook ensures a logical and practical approach to his endeavors. He is versatile in getting whatever is needed to get the goals of the NGO accomplished as such he has served as a secretary to the foundation since its inception.'),(9,'Zahra Farouk Aliyu','Welfare II','Zahra is a graduate of BSc Business Administration majored in Management Information Systems and minored in Marketing from University of Sharjah. She currently works in Setrad Nigerian Limited. Zahra is good in database, networking, programming and creating websites; she is a great organizer and loves helping people.'),(10,'Hajara Habibu Aliyu','IT Officer I','Hajara graduated from Middlesex University Dubai with a degree in information technology. Hajara is an IT specialist and philanthropist, currently working with a nongovernmental organization Partners for Development as an IT and Communication Officer. Hajara have being working in the development sector for the past 4years. This experience gave her a deep appreciation for how NGOs seek to improve the quality of life for our region, one program at a time.'),(12,'Rahila Ibrahim','Chief Pharmacist','Rahila is a pharmacist who has passion for community development services and helping people, she has strong networking skills and is a great organizer. She had volunteered for a lot of community development services among which is the Click-On Kaduna project which was aimed at introducing youth to freelancing and the virtual market'),(13,'Maryam Modibbo','Team Leader Kaduna','Maryam is a holder of B. ED degree in Islamic Education from the Islamic online University. A mother of 5 kids and a teacher by profession who loves to help the humanity with the little she has.'),(14,'Maimuna Kasim Khalid','Event Planner III','Maimuna is the CEO of mmlaban handmade slippers, she works with the Board of Internal Revenue Kano State. She holds an HND in Public Administration from Kano State Polytechnic'),(15,'Khadija Mustapha','Event Planner II','Khadija is the CEO of Khadmus Enterprises, a producer in Kannywood movie industry, she holds an HND in Account and Audit School of Management Studies in Kano'),(16,'Mackson Simon','Welfare I','Mackson is a passionate humanitarian worker who loves serving humanity. A graduate of Sociology from Bayero University Kano, he also holds a MSc in Development Studies from the same University. He currently works with one of the new generation banks'),(17,'Ummusalama Isyaka Rabiu','Treasurer','Ummusalama is an entrepreneur and a mother of two with a passion for helping the needy, she has over the years been involved in several outreaches geared towards helping the less privileged');
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `sentOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` (`id`, `name`, `email`, `message`, `sentOn`) VALUES (1,'Umar Sunusi Maitalata','maitalata@gmail.com','hELLO THERE','2019-04-24 22:43:02'),(2,'Muhammad Abubakar','muhd@yahoo.com','I love the way you do thing. Keep up the good work.','2019-04-24 22:47:15'),(3,'Abdulrahaman Nasiru','abdulrahaman12@yahoo.com','I am sending you a thank you message for all the works you have done to the needy people.','2019-04-24 23:12:04'),(4,'Amina Badamasi','amina33@yahoo.com','I am a humanitarian, I would like to join your NGO, what are the requirements for new members?','2019-04-25 16:26:18'),(5,'Maryam Abdulkadir Umar','maryam324@gmail.com','You touch the life of people may Allah reward you.','2019-04-25 16:28:53'),(6,'Hadiyatullah Yunusa Mustapha','hadi778@gmail.com','Selfless service, more grease to your elbow.','2019-04-25 16:30:21'),(7,'Aliyu Haydar Nuruddeen','haidara@gmail.com','Just want to say that I admire you people.','2019-04-25 16:33:13'),(8,'Abduljalal Bala Muhammad','abdul1721@yahoo.com','Wannan aiki da kuke na taimako Allah ne kadai ze iya biyanku. Allah ya kara muku karfin gwiwa kuyi wanda yafi wannan ma.','2019-04-25 16:40:31'),(9,'Khalid Umar Usman','khalidumar87474.gmail.com','Good work','2019-04-25 16:41:20'),(10,'Ramlat Haruna','ramlat@yahoo.com','Hello there, I would like to convey my gratitude to your selfless service.','2019-04-25 16:42:49'),(11,'Usman Usman','usman233@gmail.com','Allah ya taimaka ya kara arziki.','2019-04-25 16:45:50'),(12,'Zainab Mu\'az','zainab34@gmail.com','I would like to donate some materials','2019-04-25 16:48:52'),(13,'Jang Jang','Jang@yahoo.com','Hello there, just testing','2019-06-28 13:22:04'),(14,'TeodoraExalf','kedison2k2@gmail.com','Abbakin is a digital marketing and online advertising agency in Lagos Nigeria. We deliver your products or services to the right customers, drive more traffic and increase your sales conversions.','2019-07-06 20:42:18');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'aricomng_db'
--

--
-- Dumping routines for database 'aricomng_db'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-15  9:54:27
